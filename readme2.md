# Binar DevOps Platinum Challenge

- Nama: Fernando Mardi Nurzaman
- Tema Bootcamp: DevOps Engineer (DOE)
- wave: 2
- instruktur/pengajar: Mochammad Syaifuddin Khasani

Tujuan mengejerkan Challenge untuk mendeploy ExpressJs(backend) & ReactJs(frontend) dengan menggunakan kubernetes cluster dan membuat cloud sql di Google Cloud Provaider. dengan ada challenge ini dapat mengukur kemampuan setiap peserta bootcamp sejauh mana memahami materi yang diberikan selama bootcamp

## Langkah-langkah Pengerjaan

- [x] Membuat Dockerfile dan Docker Images
- [x] Membuat Kubernetes Cluster di Google Cloud Platform
- [x] Memasang Ingress dengan Google Cloud Load Balancer
- [x] Mengimplementasikan domain + SSl untuk ingress
- [x] Membuat Gitlab runner di Kubernetes Cluster
- [x] Melakukan Deployment aplikasi Ke Kubernetes Cluster menggunakan Gitlab CI/CD Pipeline
- [x] Membuat Dashboard Monitoring dan Logging
- [x] Menginstall Gitlab Runner dengan Ansible
- [x] Membuat VM Gitlab Runner dan Cloud SQL Menggunakan Terraform

## Gambar Rancangan Infrastructure

![](./images/archicture_gcp%20.png)

## Membuat Dockerfile dan Docker Images

Membuat Dockerfile bertujuan untuk membuat images yang akan dijalankan Didalam sebuah Container.
kalau mager pakai ini [`frontend`](./frontend/Dockerfile) [`backend`](./backend/Dockerfile)

1. Buat file dengan nama Dockerfile pada masing folder `backend` & `frontend`
2. Gunakan base images node pada masing masing Dockerfile
   ```
   FROM node:18-alpine
   ```
3. Tentukan workdir yang saat images berkerja

   ```
   WORKDIR /app/backend
   or
   WORKDIR /app/frontend
   ```

4. Copy semua file ada kedalam images
   ```
   COPY backend/. .
   or
   COPY frontend/. .
   ```
5. install depensi yang diperlukan dengan menggunakan node manegement
   ```
   RUN npm install
   ```
6. (Optional) build aplikasi dengan menggunakan node management

   ```
    RUN npm build
   ```

7. Berikan port yang berbeda supayah app bisa diakses dari dalam container
   ```
   EXPOSE 8080
   or
   EXPOSE 8081
   ```
8. Gunakan menjalankan command didalam conatainer diperlukan didalam container
   ```
   CMD ["npm","start"]
   ```
9. setelah Selesai coba build dengan command:

   ```
   docker build -t <images_name>:<tagname> .
   ```

![docker.png](./images/dockerhub.png)

## Membuat Kubernetes Cluster DI GCP

Untuk Menjalankan images yang sudah dibuat maka diperlukan sebuh container. tetapi tentu saja dengan menggunakan container saja, maka akan mengalami kesulitan apabila kita mengurus container dalam jumlah yang sangat banyak. maka kita memerlukan kubernetes untuk memangement semua container yang banyak tersebut.

1. Cari Pada pencarian Kubernetes enggine atau Menu service yang ada pada pojok kiri atas
2. Klik Create pada bagian atas atau pada bagian tengah/content
3. Setelah itu pilih standar configure dengan cara menekan configure(pada bagian standar)
4. Isi nama cluster dengan yang anda ingin sesuai dengan peraturan yang sudah ditentukan
5. Setting Region sebagai untuk diletakkannya resource anda, pilih region dan zone yang paling dekat dengan anda
6. Selanjutnya setting Node pool dengan menekat default pool ini untuk mengatur banyaknya node yang anda perlukan. anda bisa mengganti nama node-pool seperti yang anda inginkan tetapi harus mengikuti perarturan GCP
7. kamu bisa mengaktifkan cluster dengan cara mengcentang enabled cluster auto-scaler
8. setelah itu mengatur Nodes bertujuan untuk mengatur OS yang akan digunakan dan besarnya sumber resource pada setiap node
9. enabled vm sport untuk auto menghapus node yang sudah tidak terpakai lagi pada kubernetes cluster
10. tekan tombol create pada bagian bawah untuk membuat dengan config yang sudah anda lakukan sebelumnya.

![Kubernetes Cluster](./images/clustergcke.png)

## Memasang Ingress dengan Google Cloud Load Balancer

Ingress betugas untuk membuat aplikasi yang sudah dideploy dapat di akses dari luar, sedangkan Cloud Load Balancer memberikan akses keluar dari ingress dengan cara memberikan IP Ephemeral atau sementara.

1. Siapkan file dengan object ingress

   ```
   apiVersion: networking.k8s.io/v1
   kind: Ingress
   metadata:
       name: <name-ingress>
       namespace: <namespace>
   labels:
       name: <name-label-ingress>
   spec:
       rules:
           - http:
               paths:
                   - path: "/"
                   pathType: Prefix
                   backend:
                   service:
                       name: frontend-service-staging
                       port:
                           number: 8081

                     - path: "/users"
                    pathType: Prefix
                    backend:
                    service:
                        name: backend-service-staging
                        port:
                            number: 8080

   ```

2. Karena kita menggunakan Google Cloud Provaider maka hanya perlu menambahkan annotation pada ingress dibawah stage labels
   ```
   labels:
        name: <name-label-ingress>
   annotations:
       kubernetes.io/ingress.class: "gce"
   ```
3. Untuk melihat config ingress berserta Google Cloud Load Balancer [staging](./deployment/staging/ingress.yaml) & [production](./deployment/production/ingress.yaml)

## Mengimplementasikan domain + SSL untuk ingress

Mengimplementasikan domain & Ssl pada ingress hampir dengan melakukan beberapa perubahan pada ingress

1. membuat IP static pada staging & production premium dan global
   ```
   annotations:
       kubernetes.io/ingress.class: "gce"
       kubernetes.io/ingress.global-static-ip-name: "name-static-ip"
   ```
2. Tambahkan IP static pada Domain dashboard yang ada punya
   ![dasboard.png](./images/domain-dashboard.png)

3. Tambahkan juga Domain yang sudah ada pada ingress pada bagian `host:`

   ```
    spec:
        rules:
            - host: staging.testrsv.online / production.testrsv.online
              http:
                paths:
                    - path: "/"
                        pathType: Prefix
                        backend:
                        service:
                            name: frontend-service-staging
                            port:
                                number: 8081

                    - path: "/users"
                        pathType: Prefix
                        backend:
                        service:
                            name: backend-service-staging
                            port:
                                number: 8080
   ```

4. Menambahkan SSL pada inggress ada banyak tapi karena kita menggunakan Google Cloud Provaider maka biarkan Google yang me managed certificate dengan membuat file `managedcertificate.yaml` dan berisi

   ```
   apiVersion: networking.gke.io/v1
   kind: ManagedCertificate
   metadata:
       name: staging
       namespace: staging
   spec:
       domains:
       - staging.testrsv.online / production.testrsv.online
   ```

5. setelah membuat config untuk SSL maka kita harus menambahkan pemberitahuan pada `ingress.yaml `dengan menambahakn di `annotation`
   ```
    annotations:
        kubernetes.io/ingress.class: "gce"
        kubernetes.io/ingress.global-static-ip-name: "staging-static-ip"
        networking.gke.io/managed-certificates: staging
   ```
6. Untuk melihat config ingress yang sudah berisi domain + SSL pada [staging](./deployment/staging/ingress.yaml) & [production](./deployment/production/ingress.yaml)

## Membuat Gitlab runner di Kubernetes Cluster

Untuk mendeploy aplikasi yang ada repo gitlab ke Google Kubernetes Enginee di perlukan gitlab runner yang berjalan pada kubernetes cluster

1. Pastikan sudah anda sudah menginstall `Helm-Chart` pada komputer anda
2. Kunjungi [artifacthub.io](https://artifacthub.io/) yang berisi config kubernetes manifest terhadap suatu platform yang sudah tersedia
3. Search pada gitla-runner yang terverifikasi oleh gitlab langsung [ini](https://artifacthub.io/packages/helm/gitlab/gitlab-runner)
4. Copy isi Gitlab-runner manifest pada default values
5. buat file yang bernama `values.yaml` untuk menarok config Gitlab-runner manifest yang barusan di copy
6. pastikan `gitlabUrl` : https://gitlab.com/ pada no.51 dalam file `values.yaml`
7. Pastikan `rbac` : true pada no. 141 dalam file `values.yaml`
8. Tambahkan `tags`: "kubernetes-runner" pada no.359 dalam file `values.yaml`
9. Pastikan `Secret`: gitlab-secret pada no. 393 dalam file `values.yaml`
10. Buat file `secret-gitlab.yaml `dengan object secret untuk menyimpan register token runner dengan base64
    ```
    apiVersion: v1
    kind: Secret
    metadata:
        name: gitlab-secret
        namespace: gitlab-runner
    type: Opaque
        data:
            runner-registration-token: "<your_registration>" #covertkan ke base64
            runner-token:
    ```
11. Untuk menjaga keaman register_token kita encrypt ke base64

    ```
     echo -n "register_token" | base64
    ```

12. Buat juga file `namespace.yaml` untuk memisahkan project yang ada di kubernets cluster

    ```
    apiVersion: v1
    kind:  Namespace
    metadata:
        name: gitlab-runner
    ```

13. Sebelum menjalankan Gitlab-runner maka kita harus meng-apply namespace dan secret terlebih dahulu

    ```
    kubectl apply -f helm-gitlab-runner/namespace.yaml &&
    kubectl apply -f helm-gitlab-runner/secret-gitlab.yaml
    ```

14. Dan terakhir menjalankan gitlab-runner dengan menggunakan helm-chart

    ```
    helm install --namespace <namespace> gitlab-runner -f <CONFIG_VALUES_FILE> gitlab/gitlab-runner
    ```

![runner kubernetes](./images/runner-k8s.png)

## Melakukan Deployment aplikasi Ke Kubernetes Cluster menggunakan Gitlab CI/CD Pipeline

Untuk mudahkan kita dalam membuild aplikasi kita menjadi images dan mendeploy images yang sudah dibuild tanpa harus menjalankan proses build dan deploy secara manual. lebih baik menggunakan autopilot yang sudah disediakan oleh gitlab yaitu Gitlab CI/CD. Gitlab CI/CD ini akan otomatis jalan pada setiap kali developer melakukan commit ke repositori Gitlab. dengan membaca file `.gitlab-ci.yaml` pada repository yang sudah dibuat

1. terdapat 2 stage yaitu build & deploy
2. stage build merupakan proses untuk membuild Dockerfile menjadi images yang akan dipakai
3. stage deploy merupakan proses mendeploy dengan menggunakan kubernetes
4. deploy staging merupakan untuk mendeploy aplikasi pada server percobaan
5. deploy production merupakan untuk mendeploy aplikasi pada server production/real
6. serta harus menyiapkan beberapa variable yang disimpan pada Setting > Ci/CD > variables

   ![gitlab_ci/cd](./images/variable-cicd.png)

7. setelah Melakukan Commit maka akan ada perubahan dan juga memberikan trigger pada gitlab ci/cd

8. Untuk melihat apakah CI/CD itu jalan maka kamu bisa mengecek dibagian CI/CD > Pipelines

   ![gitlab_ci/cd](./images/gitlab-cicd.png)

## Membuat Dashboard Monitoring dan Logging

Monitoring kerja aplikasi dan infrastukture yang sudah kita buat, dengan menggunakan dasboard yang sudah ada serta melihat hasil logging dari GCLB

1. Membuat dashbord untuk memantau resource kubernetes Cluster
   ![dasboard.png](./images/dashboard%20monitoring.png)
2. Mencari error 400 & 500 pada GCLB
   ![staging.png](./images/staging_error.png)
   ![staging.png](./images/prod_eror.png)

## Menginstall Gitlab Runner dengan Ansible

Ansible merupakan management untuk instalation & configuration,dalam hal ini ansible digunakan sebagain instalation dan config gitlab runner pada vm

1. Buat file palybook.yaml
2. Buat file inventory.ini untuk mencatat IP address
3. Download role gitlab-runner degan menggunakan ansible galaxy
4. gunakan command dibawah ini, untuk meng-cloning config yang sudah dibuat oleh orang lain

   ```
   ansible-galaxy install riemers.gitlab-runner -p roles
   ```

5. Buat folder group_vars dan didalamnya terdapat file all.yaml

6. masukkan config yang kita ingin sesuai dengan dokumentasi seperti yang Om rimer tunjukan. [klik](https://github.com/riemers/ansible-gitlab-runner)

7. kalau malas silah copy config yang sudah aku buat dan disesuaikan

   ```

       gitlab_runner_registration_token: '<register token>'
       gitlab_runner_runners:
       - name: 'Ansible-runner'
           gitlab_runner_registration_token
           gitlab_runner_coordinator_url
           executor: docker
           docker_image: 'alpine'
           docker_volumes:
           - "/var/run/docker.sock:/var/run/docker.sock"
           - "/cache"
           extra_configs:
           runners.docker:
               memory: 1024m
           runners.docker.sysctls:
               net.ipv4.ip_forward: "1"

   ```

8. jika sudah selesai jalankan command dibawah ini untuk mengexecution config yang sudah ada

   ```
   ansible-playbook playbook.yaml -i inventory.ini
   ```

![runner ansible](./images/ansible-runner.png)

## Membuat VM Gitlab Runner dan Cloud SQL Menggunakan Terraform

Terraform membantu dalam mempercepat kerja membangun infrastructure dengan menggunakan code yang bisa dijalankan berulang kali,

1. Mendefinisikan Cloud provaider yang digunakan dalam file provaider.tf

   ```
    provider "google" {
        project = "<your project id>"
        region  = "asia-southeast2"
        zone    = "asia-southeast2-a"
    }
   ```

2. Untuk mempersiapkan module yang digunakan dengan terraform

   ```
   terraform init
   ```

3. Buat file `vm-instance.tf`

   ```
   resource "google_compute_instance" "vm_gitlab" {
        name         = "vm-gitlab-runner"
        machine_type = "e2-medium"
        zone         = "asia-southeast2-a"

        tags = ["http-server", "https-server"]
        boot_disk {
            initialize_params {
            image = "ubuntu-os-cloud/ubuntu-2204-lts"
            size  = 30
            }
        }

        metadata = {
            ssh-keys = "nando:${file("~/.ssh/id_rsa.pub")}"
        }
        network_interface {
            network = "default"
            access_config {
            }
        }
   }

   ```

4. Buat file `db-instance.tf`

   ```
    resource "google_sql_database_instance" "postgres_staging" {
        name                = "staging-postgres"
        database_version    = "POSTGRES_14"
        region              = "asia-southeast2"
        deletion_protection = false

        settings {
            tier      = "db-f1-micro"
            disk_size = 20
            ip_configuration {
            private_network = "projects/linen-rex-246117/global/networks/default"
            }
        }
   }

   resource "google_sql_database" "database_staging" {
        name     = "people-staging"
        instance = google_sql_database_instance.postgres_staging.name

   }

   resource "google_sql_user" "staging_user" {
        name     = "people-staging"
        instance = google_sql_database_instance.postgres_staging.name
        password = "people-staging"
   }
   ```

5. Cek kembali config yang sudah dibuat
   ```
   terraform validate
   ```
6. Dan pastikan kembali dengan membaca dalam plan
   ```
   terraform plan
   ```
7. Untuk dan jalankan Terraform ke GCP

   ```
   terraform apply
   ```

hasil VM Instances gitlab-runner dengan menggunakan terraform
![runner terraform](./images/vm-runner.png)
hasil membuat DB dengan menggunakan terraform
![db terraform](./images/db-terraform.png)

### Hasil Akhirnya

1. Hasil akhir bagian staging
   ![staging-last.png](./images/staging_finish.png)

2. Hasil akhir bagian production
   ![production-last.png](./images/production_finish.png)
